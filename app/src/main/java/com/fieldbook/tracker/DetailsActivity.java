package com.fieldbook.tracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

/**
 * Created by linuxlite on 5/5/16.
 */
public class DetailsActivity extends Activity {
private TextView tv;
    private ImageView image;

    @Override
    protected void onCreate(Bundle saveInstance){
        super.onCreate(saveInstance);
        setContentView(R.layout.layout);

        tv=(TextView)findViewById(R.id.txtView);
        image=(ImageView)findViewById(R.id.image);
        Bundle b = getIntent().getExtras();
        String percentage = b.getString("percent");
        String imgPath = b.getString("imgPath");
        tv.setText(percentage);
        tv.append("of Necrosis");

        Glide.with(image.getContext())
                .load(imgPath)
                .into(image);


    }

    @Override
    public void onBackPressed(){
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
    }
}
