package com.fieldbook.tracker;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailsWhitefly extends AppCompatActivity {
    private TextView tv;
    private ImageView image;

    @Override
    protected void onCreate(Bundle saveInstance){
        super.onCreate(saveInstance);
        setContentView(R.layout.whiteflycount);

        tv=(TextView)findViewById(R.id.txtView2);
        image=(ImageView)findViewById(R.id.image2);
        Bundle b = getIntent().getExtras();
        String percentage = b.getString("count");
        String imgPath = b.getString("imgPath");
        tv.setText(percentage);
        tv.append("number of whiteflies");

        Glide.with(image.getContext())
                .load(imgPath)
                .into(image);


    }

    @Override
    public void onBackPressed(){
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
    }
}
