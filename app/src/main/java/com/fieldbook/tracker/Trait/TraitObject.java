package com.fieldbook.tracker.Trait;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Simple wrapper class for trait data
 */
public class TraitObject implements Serializable{
    public String trait;
    public String format;
    public String defaultValue;
    public String minimum;
    public String maximum;
    public String details;
    public String categories;
    public String realPosition;
    public String id;


    public TraitObject(){

    }

    public TraitObject(String trait, String format, String defaultValue, String minimum, String maximum, String details, String categories, String realPosition, String id ) {
        this.trait = trait;
        this.format = format;
        this.defaultValue = defaultValue;
        this.minimum = minimum;
        this.maximum = maximum;
        this.details = details;
        this.categories = categories;
        this.realPosition = realPosition;
        this.id = id;

    }

    public static TraitObject wrap(HashMap<String, String> parameters){
        return new TraitObject(
                parameters.get("id"),
                parameters.get("format"),
                parameters.get("defaultValue"),
                parameters.get("minimum"),
                parameters.get("maximum"),
                parameters.get("details"),
                parameters.get("categories"),
                parameters.get("realPosition"),
                parameters.get("trait"));
    }

    public static HashMap<String, String> unwrap(TraitObject object){
        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("id", object.id);
        parameters.put("format", object.format);
        parameters.put("defaultValue", object.defaultValue);
        parameters.put("minimum", object.minimum);
        parameters.put("maximum", object.maximum);
        parameters.put("details", object.details);
        parameters.put("categories", object.categories);
        parameters.put("realPosition", object.realPosition);
        parameters.put("trait", object.trait);

        return parameters;
    }
}
