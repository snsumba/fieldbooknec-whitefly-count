package com.fieldbook.tracker;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Simple wrapper class for range data
 */
public class RangeObject implements Serializable {
    String range;
    String plot;
    String plot_id;

    public RangeObject() {
    }

    public RangeObject(String range, String plot, String plot_id) {
        this.range = range;
        this.plot = plot;
        this.plot_id = plot_id;
    }

    public static RangeObject wrap(HashMap<String, String> parameters){
        return new RangeObject(
                parameters.get("range"),
                parameters.get("plot"),
                parameters.get("plot_id"));
    }

    public static HashMap<String, String> unwrap(RangeObject object){
        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("range", object.range);
        parameters.put("plot", object.plot);
        parameters.put("plot_id", object.plot_id);

        return parameters;
    }
}
